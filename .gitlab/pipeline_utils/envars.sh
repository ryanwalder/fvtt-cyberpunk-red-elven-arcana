#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

# This script sets ENVARS we'll use throughout the GitLab CI pipeline.
# We set them during the 'init' job via this script rather than in
# .gitlab-ci.yml so we can conditionally set them as needed and all the
# logic is in one place.

# NOTE: All exported ENVARs MUST be uppercase!
#       Do NOT start ENVARs with 'CI_' as they are skipped in testing.

##################
# GitLab Variables
##################
# Variables that are set by GitLab CI environment
# CI_API_V4_URL, CI_PROJECT_ID, CI_COMMIT_TAG

# Set CI_COMMIT_TAG to "" if it does not exist as we use this to
# determine if we're tagging a release.
CI_COMMIT_TAG="${CI_COMMIT_TAG:-}"

##################
# System Variables
##################
# Variables directly related to the system and using by Gulp
# When adding/removing/changing these you MUST also update
# the 'gulp/conststants.mjs' file.

# MODULE_NAME is the Foundry system ID, used for paths etc.
MODULE_NAME="cprc-elven-arcana"

# MODULE_TITLE (display name for system in Foundry)
MODULE_TITLE="Cyberpunk RED: Elven Arcana"

# System manifest
MODULE_FILE="module.json"

# Changelog file
CHANGELOG_FILE="CHANGELOG.md"

########################
# Build System Variables
########################
# Variables that rarely change, can be overwritten later.

# Set the version number to the CI_COMMIT_TAG
MODULE_VERSION="${CI_COMMIT_TAG}"

# Base URL for the project
PROJECT_URL="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}"

# Define the url we upload the packages to
REPO_URL="${PROJECT_URL}/packages/generic/${MODULE_NAME}"

###################
# Dynamic Variables
###################
# Variables that need to be overwritten depending on the job needs.

# If the CI_COMMIT_TAG is empty we're probably merging to master so
# overwite the defaults. Used in the build stages.
if [[ -z "${CI_COMMIT_TAG}" ]]; then
  # Use the date/time as a version number
  MODULE_VERSION="v$(date +%Y%m%d.%H%M)"
  # Set the system title to inclue DEV to make identifying it easier in Foundry
  MODULE_TITLE="Cyberpunk RED: Elven Arcana - DEV"
  # Append "-dev" to the package repo name
  REPO_URL="${PROJECT_URL}/packages/generic/${MODULE_NAME}-dev"
fi

###################
# Derived Variables
###################
# Mostly static variables that rely on Dynamic Variables before being set.

# Full name of the release including version
RELEASE_NAME="${MODULE_NAME}-${MODULE_VERSION}"

# Set the ZIP name we'll publigh later
ZIP_FILE="${RELEASE_NAME}.zip"

##################
# Export Variables
##################
# Export the variables so we can use them later in the CI pipeline.

{
  echo "CHANGELOG_FILE=${CHANGELOG_FILE}"
  echo "PROJECT_URL=${PROJECT_URL}"
  echo "RELEASE_NAME=${RELEASE_NAME}"
  echo "REPO_URL=${REPO_URL}"
  echo "MODULE_FILE=${MODULE_FILE}"
  echo "MODULE_NAME=${MODULE_NAME}"
  echo "MODULE_TITLE=${MODULE_TITLE}"
  echo "MODULE_VERSION=${MODULE_VERSION}"
  echo "ZIP_FILE=${ZIP_FILE}"
} >vars.env
