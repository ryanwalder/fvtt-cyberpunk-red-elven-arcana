#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

# The following vars are set during the 'init' CI job.
# REPO_URL MODULE_FILE MODULE_NAME MODULE_VERSION ZIP_FILE

# Create a Release in GitLab
# NOTE: This references the files created by the `build-artifacts` job.
if ! release-cli create \
  --name "${MODULE_VERSION}" \
  --description "Automated release of ${MODULE_VERSION}" \
  --tag-name "${MODULE_VERSION}" \
  --assets-link "{\"name\":\"${MODULE_FILE}\",\"url\":\"${REPO_URL}/${MODULE_VERSION}/${MODULE_FILE}\"}" \
  --assets-link "{\"name\":\"${ZIP_FILE}\",\"url\":\"${REPO_URL}/${MODULE_VERSION}/${ZIP_FILE}\"}"; then
  # TODO: We can probably parse CHANGELOG.md and publish the changelog
  # as part of the release as a file and as the description.

  echo "❌ Unable to create release for ${MODULE_NAME} ${MODULE_VERSION}"
else
  echo "🎉 Created ${MODULE_NAME} ${MODULE_VERSION} release successfully!"
fi
