#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

# The following vars are set during the 'init' CI job.
# CHANGELOG_FILE PROJECT_URL RELEASE_NAME REPO_URL MODULE_FILE MODULE_VERSION ZIP_FILE

# Create error counter
errors=0

# Get the release from GitLab
RELEASE=$(
  curl \
    --silent \
    --location \
    "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/releases" |
    jq '.[] | select(.name=="'"${MODULE_VERSION}"'")'
)

# Get the module.json download link
RELEASE_MANIFEST=$(
  echo "${RELEASE}" |
    jq -r '.assets.links[] | select (.name=="module.json") | .url'
)
# Get the zip download link
RELEASE_DOWNLOAD=$(
  echo "${RELEASE}" |
    jq -r '.assets.links[] | select (.name=="'"${ZIP_FILE}"'") | .url'
)

# Test if we have updated the CHANGELOG.md
if grep -q "WIP" "${CHANGELOG_FILE}"; then
  echo "❌ The string 'WIP' exists in the changelog..."
  ((errors += 1))
else
  echo "✅ CHANGELOG.md has been updated!"
fi

# Check the versioned system.json url is what we expect
if [[ "${RELEASE_MANIFEST}" != "${REPO_URL}/${MODULE_VERSION}/${MODULE_FILE}" ]]; then
  echo "❌ ${MODULE_FILE} download is incorrect"
  ((errors += 1))
else
  echo "✅ ${MODULE_FILE} url in release is correct!"
fi

# Check the versioned system url is what we expect
if [[ "${RELEASE_DOWNLOAD}" != "${REPO_URL}/${MODULE_VERSION}/${ZIP_FILE}" ]]; then
  echo "❌ ${ZIP_FILE} download url is incorect."
  ((errors += 1))
else
  echo "✅ ${ZIP_FILE} url in release is correct!"
fi

set -x

# Skip for now as we haven't published yet do the file won't exist
# Check we can download the module.json
if ! curl --silent --location "${RELEASE_MANIFEST}" --output "${MODULE_FILE}"; then
  echo "❌ Unable to download ${MODULE_FILE}"
  ((errors += 1))
else
  echo "✅ ${MODULE_FILE} downloaded!"
fi

# Check we can download the zip
if ! curl --silent --location "${RELEASE_DOWNLOAD}" --output "${ZIP_FILE}"; then
  echo "❌ Unable to download ${ZIP_FILE}"
  ((errors += 1))
else
  echo "✅ ${ZIP_FILE} downloaded!"
fi

# Read the local system.json
local_version=$(jq -r .version "${MODULE_FILE}")
local_manifest=$(jq -r .manifest "${MODULE_FILE}")
local_download=$(jq -r .download "${MODULE_FILE}")
local_title=$(jq -r .title "${MODULE_FILE}")

# Check the `version` is correct in module.json
if [[ "${local_version}" != "${MODULE_VERSION}" ]]; then
  ((errors += 1))
  echo "❌ Version in ${MODULE_FILE} is incorrect"
else
  echo "✅ Version in ${MODULE_FILE} is correct!"
fi

# Check the `manifest` url is correct
if [[ "${local_manifest}" != "${REPO_URL}/latest/${MODULE_FILE}" ]]; then
  ((errors += 1))
  echo "❌ The 'manifest' url is incorrect in ${MODULE_FILE}"
else
  echo "✅ The 'manifest' url is correct in ${MODULE_FILE}!"
fi

# Check the `download` url is corect
if [[ "${local_download}" != "${REPO_URL}/${MODULE_VERSION}/${ZIP_FILE}" ]]; then
  ((errors += 1))
  echo "❌ The 'download' url is incorrect in ${MODULE_FILE}"
else
  echo "✅ The 'download' url is correct in ${MODULE_FILE}!"
fi

# Check the `title` url is corect
if [[ "${local_title}" != "${MODULE_TITLE}" ]]; then
  ((errors += 1))
  echo "❌ The 'title' url is incorrect in ${MODULE_FILE}"
else
  echo "✅ The 'title' url is correct in ${MODULE_FILE}!"
fi

if [[ ${errors} -gt 0 ]]; then
  echo "❌ Errors were detected, please see output above."
  exit 1
else
  echo "🎉 All good. Ready to release!"
fi
