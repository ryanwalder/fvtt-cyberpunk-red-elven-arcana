### PLEASE SELECT APPROPRIATE TEMPLATE FROM ABOVE BEFORE CONTINUING

Bug -> To report a bug or other issue

Feature -> To request a feature or enhancement

Localization -> To request support for another language that you can proofread

Module Support -> To report an issue with a third party module
