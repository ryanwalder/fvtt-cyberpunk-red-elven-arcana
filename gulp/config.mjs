import fs from "fs-extra";
import path from "path";
import log from "fancy-log";
import chalk from "chalk";

export const DEBUG = process.env.DEBUG ? process.env.DEBUG : false;
export const DEFAULT_DESTINATION_FOLDER = "dist";
export const SRC_DIR = "src";
export const PACKS_DIR = "packs";

export const MODULE_NAME = process.env.MODULE_NAME
  ? process.env.MODULE_NAME
  : "cprc-elven-arcana";

export const MODULE_FILE = process.env.MODULE_FILE
  ? process.env.MODULE_FILE
  : "module.json";

export const MODULE_TITLE = process.env.MODULE_TITLE
  ? process.env.MODULE_TITLE
  : "Elven Arcana";

export const MODULE_VERSION = process.env.MODULE_VERSION
  ? process.env.MODULE_VERSION
  : "v0.0.0dev";

export const SOURCE_FILES = [];

export const SOURCE_DIRS = [];

/*
 * Determines the destination directory based on the local configuration or
 * returns a default path.
 */
function _getDestDir() {
  const localConfigPath = path.resolve("foundryconfig.json");
  const localConfigExists = fs.existsSync(localConfigPath);

  if (localConfigExists) {
    const localDataPath = fs.readJSONSync(localConfigPath).dataPath;
    const dataPath = path.resolve(
      path.join(localDataPath, "Data", "modules", MODULE_NAME),
    );
    return dataPath;
  } else {
    log(
      `${chalk.yellow(
        "WARNING",
      )}: foundryconfig.json not found building to ${DEFAULT_DESTINATION_FOLDER}`,
    );
  }
  return DEFAULT_DESTINATION_FOLDER;
}

export const DEST_DIR = _getDestDir();
