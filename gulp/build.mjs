import fs from "fs-extra";
import log from "fancy-log";
import gulp from "gulp";
import path from "path";

import {
  DEBUG,
  DEST_DIR,
  SRC_DIR,
  SOURCE_FILES,
  SOURCE_DIRS,
  MODULE_FILE,
  MODULE_TITLE,
  MODULE_VERSION,
} from "./config.mjs";

// Helter function to create the target directory we're building into
async function _createDist() {
  if (!fs.existsSync(DEST_DIR)) {
    fs.mkdirSync(DEST_DIR);
  }
}

// Blast the build directory to ensure it's fresh
async function cleanDist() {
  if (fs.existsSync(DEST_DIR)) {
    fs.emptyDirSync(DEST_DIR);
  }
}

// Copy all static assets into the build directory
// defined in `./config.mjs`
async function copyAssets() {
  return new Promise((cb) => {
    log("Copying static assets...");
    _createDist();
    [...SOURCE_FILES, ...SOURCE_DIRS].forEach((asset) => {
      if (DEBUG) {
        log(`DEBUG: Copying ${asset.from}`);
      }
      gulp.src(asset.from).pipe(gulp.dest(path.resolve(DEST_DIR, asset.to)));
    });
    log("Finished copying static assets.");
    cb();
  });
}

async function buildManifest() {
  return new Promise((cb) => {
    log(`Building ${MODULE_FILE}...`);
    _createDist();
    // Read the template module.json from src/
    const moduleRaw = fs.readFileSync(path.resolve(SRC_DIR, MODULE_FILE));
    const module = JSON.parse(moduleRaw);
    // If we're in CI use $VERSION as the version, else use a dummy version
    const version = MODULE_VERSION;
    // Construct some URLs
    const repoUrl = process.env.CI
      ? process.env.REPO_URL
      : "http://example.com";
    const zipFile = process.env.CI ? process.env.ZIP_FILE : "cpr.zip";
    const manifestUrl = `${repoUrl}/latest/${MODULE_FILE}`;
    const downloadUrl = `${repoUrl}/${version}/${zipFile}`;

    module.version = version;
    module.manifest = manifestUrl;
    module.download = downloadUrl;
    module.title = MODULE_TITLE;

    fs.writeFileSync(
      path.resolve(DEST_DIR, MODULE_FILE),
      JSON.stringify(module, null, 2),
    );
    log(`Finished building ${MODULE_FILE}.`);
    cb();
  });
}

async function processImages() {
  return new Promise((cb) => {
    log("Processing Images...");
    gulp
      .src("src/**/*.{jpg,jpeg,png,webp,webm}", { base: SRC_DIR })
      .on("data", (file) => {
        if (DEBUG) {
          log(
            `DEBUG: Processing Image: ${path.relative(
              process.cwd(),
              file.path,
            )}`,
          );
        }
      })
      .pipe(gulp.dest(DEST_DIR))
      .on("finish", () => {
        log("Finished Processing Images.");
        cb();
      });
  });
}

async function watchSrc() {
  // Helper - watch the pattern, copy the output on change
  function watcher(pattern, out) {
    gulp
      .watch(pattern)
      .on("all", () =>
        gulp.src(pattern).pipe(gulp.dest(path.resolve(DEST_DIR, out))),
      );
  }

  SOURCE_FILES.forEach((file) => watcher(file.from, file.to));
  SOURCE_DIRS.forEach((folder) => watcher(folder.from, folder.to));
  gulp.watch("src/**/*.less").on("all", () => compileLess());
  // disabling while we fix Crowdin
  // gulp.watch("src/lang/*.json").on("all", () => propagateLangs());
  gulp
    .watch("src/**/*.{jpeg,jpg,png,webp,webm}")
    .on("all", () => processImages());
}

export { buildManifest, cleanDist, copyAssets, watchSrc, processImages };
