/* eslint-disable no-param-reassign  */
import fs from "fs-extra";
import path from "path";
import sanitize from "sanitize-filename";
import YAML from "js-yaml";
import { ClassicLevel } from "classic-level";

import { SRC_DIR, MODULE_FILE, MODULE_VERSION } from "../config.mjs";

/*
 * PackUtils; helpful functions for dealing with packs
 */
export default class PackUtils {
  /**
   * Extracts the pack type from a levelDB key.
   *
   * The input string is expected to be in the format:
   *
   * '!tables!4S3emhUCIIDNoTAg'
   *
   * @param {string} str - The input string from which the pack type should be
   *                       extracted.
   * @returns {string} The extracted pack type from the input string.
   *
   */
  static getPackType(str) {
    return str.split("!")[1].split(".")[0];
  }

  /*
   * Cleans the given filename by removing or replacing specific characters
   * and symbols.
   *
   * Takes a string representing a filename and performs clean-up then converts
   * the filename to lowercase.
   *
   * @param {string} data - The input filename to be cleaned.
   * @returns {string} - The cleaned filename with the specified characters
   *                     removed or replaced.
   */
  static cleanFileName(str) {
    return sanitize(str)
      .replace(/\u2060/gu, "") // invisible whitespace
      .replace(/\u2211/g, "") // Σ
      .replace(/\u03B2/g, "") // β
      .replace(/[‘’]/gu, "")
      .replace(/[“”]/gu, "")
      .replace(/['"]/g, "")
      .replace(/[()]/g, "")
      .replace(/[&]/g, "and")
      .replace(/•/g, ".")
      .replace(/ /g, ".")
      .replace(/,/g, "")
      .replace(/\.\./g, ".")
      .replace(/\.-\./g, ".");
  }

  /*
   * Removes invisible whitespace characters and normalizes single and double
   * quotes in the given string.
   *
   * This function takes a string as input and performs the following operations:
   * 1. Removes invisible whitespace characters (U+2060).
   * 2. Replaces curly single quotes (‘’) with straight single quotes (').
   * 3. Replaces curly double quotes (“”) with straight double quotes (").
   *
   * @param {string} str - The input string to be cleaned.
   * @returns {string} - The cleaned string with invisible whitespace characters
   *                     removed and quotes normalized.
   */
  static cleanString(str) {
    return str
      .replace(/\u2060/gu, "")
      .replace(/[‘’]/gu, "'")
      .replace(/[“”]/gu, '"');
  }

  /**
   * Removes packs from the array based on their names.
   * @param {Array} packs - The array of packs.
   * @param {Array} packNamesToRemove - The array of pack names to remove.
   * @returns {Promise<Array>} - The updated array of packs after removal.
   */
  static removePacksByName(packs, packNamesToRemove) {
    return packs.filter((pack) => {
      return !packNamesToRemove.includes(pack.name);
    });
  }

  /*
   * Determine the name of the YAML fragment of an extracted pack
   *
   * @param {object} data - The fragment data
   * @returns {string} - the calculated fragment filename
   */
  static getFragmentName(data) {
    console.log(data);
    // Determine the type of document we are dealing with from the _key
    const foundryType = this.getPackType(data._key);
    // If it's an item work out what type of item it is
    const moduleType = data.type ? data.type.toLowerCase() : "";

    // We truncate the filename to 5 words to limit filename length
    // and cast to lowercase for consistency
    const name = data.name
      ? data.name.split(" ").slice(0, 5).join(" ").toLowerCase()
      : "";

    switch (foundryType) {
      case "items":
        return this.cleanFileName(`${moduleType}.${name}.yaml`);
      case "macros":
        return this.cleanFileName(`macro.${name}.yaml`);
      case "tables":
        return this.cleanFileName(`rolltable.${name}.yaml`);
      case "scenes":
        return this.cleanFileName(`scene.${name}.yaml`);
      case "folders":
        return this.cleanFileName(`folder.${name}.yaml`);
      case "journal":
        return this.cleanFileName(`journal.${name}.yaml`);
      case "actors":
        return this.cleanFileName(`actor.${name}.yaml`);
      default:
        throw Error(`Could not determine the type of db entry: ${foundryType}`);
    }
  }

  /*
   * Reads YAML fragments in a directory and writes them to a levelDB pack
   *
   * Inspired/taken from FoundryVTT-CLI:
   * https://github.com/foundryvtt/foundryvtt-cli/blob/main/commands/package.mjs
   *
   * This function reads YAML fragments located at the specified `packPath` and
   * packs it's content into a levelDB specified by `outputDir`.
   *
   * @param {string} packDir - The path to the LevelDB pack to be unpacked.
   * @param {string} fragmentDir - The output directory where individual YAML
   *                               files will be written.
   * @returns {Promise<void>} - A Promise that resolves when the unpacking process
   *                           is completed.
   */
  static async packLeveldb(fragmentDir, packDir) {
    const db = new ClassicLevel(packDir, {
      keyEncoding: "utf8",
      valueEncoding: "json",
    });
    const batch = db.batch();

    const files = fs.readdirSync(fragmentDir);
    for (const file of files) {
      const fileContents = fs.readFileSync(
        path.join(fragmentDir, file),
        "utf-8",
      );
      const data = YAML.load(fileContents);
      const key = data._key;
      // We don't want to store the key in the data so delete it from data
      delete data._key;
      // Scrub the data of anything we don't need
      const cleanData = this.cleanPackData(data);
      // Generate the `_stats` key
      const finalData = this.generateStats(cleanData);
      // Add the data to the batch to be written to the db
      batch.put(key, finalData);
    }

    // Write to the db
    await batch.write();
    await db.close();
  }

  /**
   * Generate statistics for the provided data by adding metadata related to the
   * data's origin and modifications. This method adds information such as core
   * version, creation time, modification details, and module version to the input
   * data.
   *
   * @param {object} data - The data object to which statistics and metadata will
   *                       be added.
   * @returns {object} A new object containing the input data along with added
   *                   statistics and metadata.
   *
   */
  static generateStats(data) {
    const sysFile = JSON.parse(
      fs.readFileSync(path.resolve(SRC_DIR, MODULE_FILE)),
    );
    const foundryVersion = sysFile.compatibility.minimum;
    const timestamp = new Date().getTime();

    const stats = {
      _stats: {
        coreVersion: foundryVersion,
        createdTime: timestamp,
        lastModifiedBy: "00CPRCBuildBot00",
        modifiedTime: timestamp,
        moduleVersion: MODULE_VERSION,
      },
    };

    return { ...data, ...stats };
  }

  /**
   * Cleans the given pack data by removing unnecessary properties and fixing
   * common errors.
   *
   */
  static cleanPackData(data) {
    log(`DEBUG: cleanPackData called`);
    return data;
  }
}
