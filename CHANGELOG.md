<!-- markdownlint-disable MD024 -->

# Changelog

## Version 0.0.1

Initial Release

- Add Mounts
- Add Journal
- Add new Gear
- Add Treasure Chests
  - Add Treasure Chest Items
- Add Spells
- Add Roles
- Add Macros
